import java.io.*;
import java.util.*;

public class Monster {
    private String monsterName;
    private int hp;
    private int atk;
    private int exp;
    private int money;

    public Monster() {
        monsterName = "";
        hp = 0;
        atk = 0;
        exp = 0;
        money = 0;
    }
    public void setGreenSlime() {
        monsterName = "Green Slime";
        hp = 10;
        atk = 1;
        exp = 10;
        money = 10;
    }
    public void setRedSlime() {
        monsterName = "Red Slime";
        hp = 20;
        atk = 5;
        exp = 20;
        money = 30;
    }
    public String getMonsterName() {
        return monsterName;
    }
    public int getMonsterHp() {
        return hp;
    }
    public int getMonsterAtk() {
        return atk;
    }
    public int getMonsterExp() {
        return exp;
    }
    public int getMonsterMoney() {
        return money;
    }
    public int noviceAttackMonster(int noviceDamage) {
        return hp = hp - noviceDamage;
    }
    public void itemTakeDamageToMonster(int damagePoint) {
        hp = hp - damagePoint;
    }
}