import java.io.*;
import java.util.*;

public class Gameplay {
    public static void main(String  agr[]) {
        Novice novice = new Novice();
        Monster monster = new Monster();
        Interface playerInterface = new Interface();
        Scanner reader = new Scanner(System.in);

        System.out.println("Welcome to New World!");
        System.out.print("Enter Your name: ");
        novice.setNoviceName(reader.nextLine()); //set Novice's Name

        while(true){ //main

            playerInterface.mainInterface(novice); //show main interface
            System.out.println("Choose Your choice: ");
            int mainChoice = reader.nextInt(); //choose mainchoice

            if(mainChoice == 1){
                while(true){

                    playerInterface.storeInterface(); //show store interface
                    System.out.println("Choose Your choice: ");
                    int choiceForBuy = reader.nextInt(); //choose choiceforbuy

                    if(choiceForBuy == 1) { //buypotion
                        if (novice.bag.checkFullBag() >= novice.bag.getBagCapacity()){
                            System.out.println("Bag is Full");
                        }
                        else {
                        novice.bag.putPotionInBag();
                        novice.buyItem(20);
                        }
                    }
                    else if(choiceForBuy == 2) { //buymaxpotion
                        if (novice.bag.checkFullBag() >= novice.bag.getBagCapacity()){
                            System.out.println("Bag is Full");
                        }
                        else {
                        novice.bag.putMaxPotionInBag();
                        novice.buyItem(100);
                        }
                    }
                    else if(choiceForBuy == 3) { //buykunai
                        if (novice.bag.checkFullBag() >= novice.bag.getBagCapacity()){
                            System.out.println("Bag is Full");
                        }
                        else {
                        novice.bag.putKunaiInBag();
                        novice.buyItem(100);
                        }
                    }
                    else if(choiceForBuy == 4) {
                        break;
                    }
                    
                }
            }

            else if (mainChoice == 2) {
                novice.bag.showItems();
            }
            else if (mainChoice == 3) {
                playerInterface.huntInterface();
                System.out.println("Choose Your choice: ");
                int choiceForHunt = reader.nextInt();
                if(choiceForHunt == 1){
                    monster.setGreenSlime();
                }
                else if(choiceForHunt == 2){
                    monster.setRedSlime();
                }
                else if(choiceForHunt == 3){
                    break;
                }

                while (monster.getMonsterHp()>0){
                    novice.monsterAttackNovice(monster.getMonsterAtk());
                    monster.noviceAttackMonster(novice.getAtk());

                    if(novice.getHp() <= 0){
                        System.out.println("You Died Pleas try again");
                        break;
                    }
                }
                if (novice.getHp() >=1 ){
                    novice.expGain(monster.getMonsterExp());
                    novice.monsterMoneyDrop(monster.getMonsterMoney());
                }
                else if(novice.getHp()<0){
                    System.out.println("Reborn");
                    novice.reborn();
                }
                novice.levelUp(novice.getExp(), novice.getLevel());
            }

            else if(mainChoice == 4) {
                break;
            }
        }
    }
}
