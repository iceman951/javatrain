import java.io.*;
import java.util.*;

public class Novice {
    public String noviceName;
    private int hp;
    private int maxHp;
    private int level;
    private int atk;
    private int exp;
    private int money;
    private int mana;
    private int healSkill;
    Bag bag;
    public Novice(){
       noviceName = "";
       maxHp = 40;
       hp = maxHp;
       level = 1;
       atk = 5;
       healSkill = 5;
       exp = 0;
       money = 500;
       mana = 20;
       bag = new Bag();
    }

    public int getHp() {
        return hp;
    }
    public int getMaxHp() {
        return maxHp;
    }
    public int getLevel() {
        return level;
    }
    public int getAtk() {
        return atk;
    }
    public int getExp() {
        return exp;
    }
    public int getMoney() {
        return money;
    }
    public int getMana() {
        return mana;
    }
    public void buyItem(int cost) {
        if (money < cost){
            System.out.println("Not Enought Money");
        }
        else    {
            money = money - cost;
            System.out.println("Thaks For Purchased");
        }
    }
    public void setNoviceName(String name) {
        noviceName = name;
    }
    public String getNoviceName() {
        return noviceName;
    }
    public void expGain(int expgain) {
        exp = exp + expgain;
    }
    public void monsterMoneyDrop(int dropMoney) {
        money = money + dropMoney;
    }
    public int monsterAttackNovice(int monsterDamage) {
        return hp = hp - monsterDamage;
    }
    public void reborn() {
        hp = maxHp;
        exp = 0;
        money = money/2;
    }
    public void heal() {
        hp = hp + healSkill;
        if(hp>maxHp) hp = maxHp;
    }
    public void healNoviceWithItem(int healPoint) {
        hp = hp + healPoint;
        if(hp>maxHp) hp = maxHp;
    }
    public void levelUp(int lvlupExp, int lvlupLevel) {
        if (lvlupExp > lvlupLevel*50){
        level++;
        atk++;
        exp = 0;
        maxHp = maxHp+10;
        hp = maxHp;
        System.out.println("Congrat!! now your Level is " + level);
        }
    }
    
}