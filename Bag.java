import java.io.*;
import java.util.*;

public class Bag {
    ArrayList<Item> items;
    int capacity;

    public Bag(){
        capacity = 5;
        items = new ArrayList<Item>();
    }

    public void putPotionInBag(){
        items.add(new Item(0));
    }
    public void putMaxPotionInBag(){
        items.add(new Item(1));
    }
    public void putKunaiInBag(){
        items.add(new Item(2));
    }

    public void showItems() {
        int itemNumber = 1;
        System.out.println("*************************");
        System.out.println("Your Bag has: ");
        for (Item item : items) {
            System.out.println(itemNumber + "." + item.itemName);
            itemNumber++;
        }
        System.out.println("*************************");
    }

    public int checkFullBag() {
        return items.size();
    }
    public int getBagCapacity() {
        return capacity;
    }
}