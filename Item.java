import java.io.*;
import java.util.*;

public class Item {
    String itemName;
    int heal;
    int damage;

    public Item(int index){
        // Item newItem = new Item();
        if(index == 0){
            itemName = "Potion";
            heal = 20;
            damage = 0;
        }
        else if (index == 1) {
            itemName = "MaxPotion";
            heal = 9999;
            damage = 0;
        }
        else if (index == 2) {
            itemName = "Kunai";
            heal = 0;
            damage = 100;
        }
        // return newItem;
    }

}